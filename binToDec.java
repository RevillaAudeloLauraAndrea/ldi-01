
/**
 * Write a description of class binarioDecimal here.
 * 
 * @author (andreaRevilla) 
 * @version (a version number or a date)
 */

import java.util.Scanner;
public class binToDec{
public void binarioDecimal()
    {
        int numero = 0;
        int contador = 1;
        int cifraBinaria = 0;
        System.out.println("Por favor introduzca un número binario");
        Scanner entrada= new Scanner(System.in);
        numero = entrada.nextInt();
        
        //mientras numero sea mayor o igual a 1 entra al ciclo         
        while (numero>=1)
        {
            
            cifraBinaria +=  (numero%10 * cuadradoPosicion(contador));
            //incrementamos el contador
            contador++;
            //dividimos a numero entre diez y el resultado será tomado como el nuevo valor para ser evaluado en el while
            numero/=10;     
        }
        
    
    System.out.println("El número binario es:" + cifraBinaria);
    }
    
    public int cuadradoPosicion(int cuadrado)
    {
        int contador = 1;
        int acomulador = 1;
        //evaluamos el while en la condición controlado por el numero "cuadrado"
        //si el valor es 1 no entra al ciclo
        while (contador<cuadrado)
        {
            //se acomula la multiplicación de 1 * 2= 2 ; 2*2 = 4 .....
            acomulador *= 2;
            contador++;
        }        
        return acomulador;
        
    }
}